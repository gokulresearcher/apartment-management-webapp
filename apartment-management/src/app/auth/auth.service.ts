import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
        .append('Accept', 'application/json')
  };

  constructor(private httpClient: HttpClient) { }

  isAuthenticated(data): boolean {
    console.log('In Auth');

    this.callApi(data).subscribe(
        response => {
          console.log('data', response);
          return true;
        },
        err => {
          console.log('ERROR:' + err);
          return false;
        }
    );

    return true;
  }

  callApi(data) {
    return this.httpClient
        .post('https://postb.in/1577430998693-7997362697497', JSON.stringify(data), this.httpOptions)
        .pipe(
            map(rxData => rxData),
            catchError(this.errorHandler)
        );
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error.message)  || 'Server error';
  }
}
