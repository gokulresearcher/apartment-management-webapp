import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  private formData: FormGroup;

  constructor(private router: Router, private authService: AuthService) {

  }

  ngOnInit() {
    this.formData = new FormGroup({
      username: new FormControl(),
      password: new FormControl()
    });
  }

  login() {
    const flag = this.authService.isAuthenticated(this.formData.value);
    console.log('FLAG1:', flag);
    if (!flag) {
      console.log('FLAG2:', flag);
      this.router.navigate(['/dashboard']);
    }
  }

}
